package org.training.bank.application.repository;

import java.util.Optional;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.BeneficiaryAccount;

public interface BeneficiaryAccountRepository extends JpaRepository<BeneficiaryAccount, Long> {
	
	Optional<BeneficiaryAccount> findByDestinationAccountNumber(String destinationAccountNumber);

	List<BeneficiaryAccount> findByAccount(Account account);

}
