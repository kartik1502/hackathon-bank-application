package org.training.bank.application.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class BeneficiaryAccount {

	@Id
	private long beneficiaryId;
	
	private String destinationAccountNumber;
	
	private String beneficiaryName;
	
	@ManyToOne
	@JoinColumn(name = "accountId")
	private Account account;
}
