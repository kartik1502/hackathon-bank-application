package org.training.bank.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.FundTransferRequest;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.dto.ValidateTransfer;
import org.training.bank.application.service.FundTransferService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/fundtransfers")
@RequiredArgsConstructor
public class FundTransferController {

	private final FundTransferService fundTransferService;
	
	@PostMapping
	public ResponseEntity<ResponseDto> initiateTransfer(@RequestBody @Valid FundTransferRequest request) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(fundTransferService.initiateTransfer(request));
	}
	
	@PutMapping
	public ResponseEntity<ResponseDto> validateTransfer(@RequestBody ValidateTransfer transfer) {
		return ResponseEntity.ok()
				.body(fundTransferService.validateTransfer(transfer));
	}
	
	
}
