package org.training.bank.application.dto;

import jakarta.validation.constraints.Pattern;

public record LoginDto(@Pattern(regexp = "[A-Za-z0-9]+",message="CustomerId should contain both Alphabets and digits and shouldn't contain any spaces") String customerId,String password) {

}
