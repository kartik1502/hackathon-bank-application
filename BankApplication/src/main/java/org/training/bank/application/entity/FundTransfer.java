package org.training.bank.application.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FundTransfer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long fundtransferId;
	
	private String customerId;
	
	private String referenceId;
	
	private String fromAccountNumber;
	
	private String toAccountNumber;
	
	private BigDecimal amount;
	
	@Enumerated(EnumType.STRING)
	private TransferType transferType;
	
	@CreationTimestamp
	private LocalDateTime transferAt;
	
	private String otpToken;
	
	@Enumerated(EnumType.STRING)
	private TransferStatus transferStatus;
}
