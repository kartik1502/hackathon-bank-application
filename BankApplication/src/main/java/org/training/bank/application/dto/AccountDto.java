package org.training.bank.application.dto;

import java.math.BigDecimal;

public record AccountDto(String name,String accountNumber,BigDecimal balance,String accountType) {

}
