package org.training.bank.application.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.service.BeneficiaryService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class BeneficiaryController {
	
	private final BeneficiaryService beneficiaryService;
	
	
	@GetMapping("/beneficiary-accounts/{accountId}")
	public ResponseEntity<List<BeneficiaryDto>> viewBeneficiary(@PathVariable String accountId) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(beneficiaryService.viewBeneficiary(accountId));
	}

}
