package org.training.bank.application.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.training.bank.application.dto.OtpRequest;
import org.training.bank.application.dto.OtpResponse;

@FeignClient(url = "https://prod-06.centralindia.logic.azure.com:443/workflows/a6cee14bdc1f4dfc95b1add34115b0e6/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=wdV2KuUywfZIYjr_DajgjrUkRKveUkGnqk7uBoGbreU", value = "otp-service")
public interface OtpService {

	@PostMapping
	OtpResponse getOtpToken(@RequestBody OtpRequest otpRequest);
}
