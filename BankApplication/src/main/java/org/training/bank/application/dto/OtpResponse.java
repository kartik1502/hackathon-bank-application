package org.training.bank.application.dto;

public record OtpResponse(String otp) {

}
