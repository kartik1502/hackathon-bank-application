package org.training.bank.application.service.implementation;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.training.bank.application.dto.FundTransferRequest;
import org.training.bank.application.dto.OtpRequest;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.dto.ValidateTransfer;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.BeneficiaryAccount;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.FundTransfer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.entity.TransactionDetails;
import org.training.bank.application.entity.TransferStatus;
import org.training.bank.application.entity.TransferType;
import org.training.bank.application.exception.InSufficientFund;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.exception.ValidationFalied;
import org.training.bank.application.feign.client.OtpService;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.repository.FundTransferRepository;
import org.training.bank.application.repository.TransactionDetailsRepository;
import org.training.bank.application.service.FundTransferService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class FundTransferServiceImpl implements FundTransferService {
	
	private final CustomerRepository customerRepository;
	
	private final FundTransferRepository fundTransferRepository;
	
	private final AccountRepository accountRepository;
	
	private final BeneficiaryAccountRepository beneficiaryAccountRepository;
	
	private final OtpService otpService;
	
	private final TransactionDetailsRepository transactionDetailsRepository;
	
	@Value("${spring.application.response.ok}")
	private String responseOk;

	@Override
	public ResponseDto initiateTransfer(FundTransferRequest request) {
		
		
		Customer customer = customerRepository.findById(request.customerId())
				.orElseThrow(() -> {
					log.warn("Requested customer not found");
					return new ResourceNotFound("Customer not found");
				});
		
		if(customer.getLoginStatus().equals(LoginStatus.LOGOUT)) {
			throw new UnAuthorizedAccess("Need to login, to use the bank applicaiton");
		}
		Account fromAccount = accountRepository.findByAccountNumber(request.fromAccount())
				.orElseThrow(() -> {
					log.warn("Invalid from Account number");
					return new ResourceNotFound("Requested account not found");
				});
		
		if(fromAccount.getBalance().compareTo(request.amount()) < 0) {
			throw new InSufficientFund();
		}
		
		BeneficiaryAccount toAccount = beneficiaryAccountRepository.findByDestinationAccountNumber(request.toAccount())
				.orElseThrow(() -> {
					log.warn("Beneficiary account not found");
					throw new ResourceNotFound("Requested destination account not found");
				});
		
		FundTransfer fundTransfer = FundTransfer.builder()
				.customerId(request.customerId())
				.referenceId(UUID.randomUUID().toString())
				.fromAccountNumber(fromAccount.getAccountNumber())
				.toAccountNumber(toAccount.getDestinationAccountNumber())
				.amount(request.amount())
				.transferType(TransferType.valueOf(request.transferType()))
				.transferStatus(TransferStatus.PENDING)
				.otpToken(otpService.getOtpToken(new OtpRequest("karthikbanu.kulkarni@hcl.com")).otp()).build();
		
		fundTransferRepository.save(fundTransfer);
		
		return new ResponseDto(responseOk, "Fund transfer initiated, validate the transfer");
	}

	@Override
	public ResponseDto validateTransfer(ValidateTransfer transfer) {
		
		Customer customer = customerRepository.findById(transfer.customerId())
				.orElseThrow(() -> {
					log.warn("Requested customer not found");
					return new ResourceNotFound("Customer not found");
				});
		
		if(customer.getLoginStatus().equals(LoginStatus.LOGOUT)) {
			throw new UnAuthorizedAccess("Need to login, to use the bank applicaiton");
		}
		
		FundTransfer fundTransfer = fundTransferRepository.findByReferenceId(transfer.referenceId())
				.orElseThrow(() -> {
					log.warn("Invalid refernceId");
					return new ResourceNotFound("Requested fund transfer not found");
				});
		
		if(!fundTransfer.getOtpToken().equals(transfer.otpToken())){
			fundTransfer.setTransferStatus(TransferStatus.FAILED);
			fundTransferRepository.save(fundTransfer);
			throw new ValidationFalied();
		}
		fundTransfer.setTransferStatus(TransferStatus.SUCCESS);
		List<TransactionDetails> transactions = List.of(
				TransactionDetails.builder().accountNumber(fundTransfer.getFromAccountNumber())
				.amount(fundTransfer.getAmount().negate())
				.referenceId(fundTransfer.getReferenceId())
				.transferType(TransferType.DEBIT).build() ,
				
				TransactionDetails.builder().accountNumber(fundTransfer.getToAccountNumber())
				.amount(fundTransfer.getAmount())
				.referenceId(fundTransfer.getReferenceId())
				.transferType(TransferType.CREDIT).build());
		
		Account fromAccount = accountRepository.findByAccountNumber(fundTransfer.getFromAccountNumber())
				.orElseThrow(() -> {
					log.warn("Invalid from Account number");
					return new ResourceNotFound("Requested account not found");
				});
		
		fromAccount.setBalance(fromAccount.getBalance().subtract(fundTransfer.getAmount()));
		fundTransferRepository.save(fundTransfer);
		transactionDetailsRepository.saveAll(transactions);
		accountRepository.save(fromAccount);
		
		return new ResponseDto(responseOk, "Transfer validated");
	}

	
}
