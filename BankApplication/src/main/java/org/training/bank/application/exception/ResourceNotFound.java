package org.training.bank.application.exception;

public class ResourceNotFound extends GlobalException{

	private static final long serialVersionUID = 1L;

	public ResourceNotFound(String message) {
		super(message, GlobalErrorCode.NOT_FOUND);
	}

	public ResourceNotFound() {
		super("resource not found", GlobalErrorCode.NOT_FOUND);
	}
	

}
