package org.training.bank.application.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account {
	
	@Id
	private String accountId;
	
	private String accountNumber;
	
	private LocalDate creationDate;
	
	private BigDecimal balance;
	
	@Enumerated(EnumType.STRING)
	private AccountType accountType;
	
	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;
}
