package org.training.bank.application.dto;


public record ResponseDto(String responseCode,String responseMessage) {

}
