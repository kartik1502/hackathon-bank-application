package org.training.bank.application.service.implementation;

import org.springframework.stereotype.Service;
import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.InvalidOperation;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.service.CustomerService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements CustomerService{
	
	
	
	private final CustomerRepository customerRepository;
	
	
	
	@Override
	public ResponseDto login(LoginDto loginDto) {

		 
		Customer customer=customerRepository.findById(loginDto.customerId())
				
				.orElseThrow(() -> new ResourceNotFound("Customer Not Found"));
		
		if(!customer.getPassword().equals(loginDto.password()))
		{
			log.error("Invalid Credentials");
			throw new UnAuthorizedAccess("Invalid Credentials");
		}
		if(customer.getLoginStatus().equals(LoginStatus.LOGIN))
		{
			throw new InvalidOperation("Customer already Logged In");
		}
		customer.setLoginStatus(LoginStatus.LOGIN);
		customerRepository.save(customer);
		log.info("User Logged in successfully"); 
		return new ResponseDto("200","Login successful");
	}

}
