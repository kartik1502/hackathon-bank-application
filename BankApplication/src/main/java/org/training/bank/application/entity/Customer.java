package org.training.bank.application.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Customer {

	@Id
	private String customerId;
	
	private String name;
	
	private String aadhar;
	
	private String emailId;
	
	private String contactNo;
	
	private String password;
	
	@Enumerated(EnumType.STRING)
	private LoginStatus loginStatus;
}
