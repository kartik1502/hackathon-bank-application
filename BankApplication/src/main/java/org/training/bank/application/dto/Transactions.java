package org.training.bank.application.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record Transactions(String accountNumber, 
		BigDecimal amount, 
		LocalDateTime transactionDate, 
		String transactionType) {

}