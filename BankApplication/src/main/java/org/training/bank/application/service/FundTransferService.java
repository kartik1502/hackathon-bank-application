package org.training.bank.application.service;

import org.training.bank.application.dto.FundTransferRequest;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.dto.ValidateTransfer;

public interface FundTransferService {

	ResponseDto initiateTransfer(FundTransferRequest request);

	ResponseDto validateTransfer(ValidateTransfer transfer);

}