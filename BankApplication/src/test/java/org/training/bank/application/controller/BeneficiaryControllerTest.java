package org.training.bank.application.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.service.BeneficiaryService;

@ExtendWith(SpringExtension.class)
class BeneficiaryControllerTest {
	@Mock
	private BeneficiaryService beneficiaryService;
	@InjectMocks
	private BeneficiaryController beneficiaryController;
	
	
	@Test
    void testViewBeneficiary() {
        
        List<BeneficiaryDto> dtos = new ArrayList<>();
        
        dtos.add(new BeneficiaryDto("DGH5667", null));
        
        Mockito.when(beneficiaryService.viewBeneficiary("DGH5667"))
                .thenReturn(dtos);
        ResponseEntity<List<BeneficiaryDto>> beneficiaryDtos = beneficiaryController.viewBeneficiary("DGH5667");
        assertEquals(1, beneficiaryDtos.getBody().size());
    }

}
