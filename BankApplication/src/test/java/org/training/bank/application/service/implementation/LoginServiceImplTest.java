package org.training.bank.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.InvalidOperation;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
class LoginServiceImplTest {

	@InjectMocks
	private LoginServiceImpl loginServiceImpl;

	@Mock
	private CustomerRepository customerRepository;

	@Test
	void login_PositiveTest() {

		Customer customer = new Customer();
		customer.setCustomerId("COO1");
		customer.setPassword("Divya@514");
		customer.setLoginStatus(LoginStatus.LOGOUT);
		LoginDto loginDto = new LoginDto("C001", "Divya@514");
		when(customerRepository.findById(loginDto.customerId())).thenReturn(Optional.of(customer));
		ResponseDto response = loginServiceImpl.login(loginDto);
		assertNotNull(response);
		assertEquals("200", response.responseCode());
		assertEquals("Login successful", response.responseMessage());

	}

	@Test
	void login_NegativeTest_CustomerNotFound() {
		Customer customer = new Customer();
		customer.setCustomerId("COO1");
		customer.setPassword("Divya@514");
		LoginDto loginDto = new LoginDto("C001", "Divya@514");

		when(customerRepository.findById(loginDto.customerId())).thenReturn(Optional.empty());

		ResourceNotFound exception = assertThrows(ResourceNotFound.class, () -> loginServiceImpl.login(loginDto));
		assertEquals("Customer Not Found", exception.getMessage());

	}

	@Test
	void login_NegativeTest_AccessDenied() {
		Customer customer = new Customer();
		customer.setCustomerId("COO1");
		customer.setPassword("Divya@514");
		LoginDto loginDto = new LoginDto("C001", "Divya514");

		when(customerRepository.findById(loginDto.customerId())).thenReturn(Optional.of(customer));

		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class, () -> loginServiceImpl.login(loginDto));
		assertEquals("Invalid Credentials", exception.getMessage());

	}

	@Test
	void alreadyLoggedIn() {
		Customer customer = new Customer();
		customer.setCustomerId("COO1");
		customer.setPassword("Divya@514");
		customer.setLoginStatus(LoginStatus.LOGIN);

		LoginDto loginDto = new LoginDto("C001", "Divya@514");

		when(customerRepository.findById(loginDto.customerId())).thenReturn(Optional.of(customer));

		InvalidOperation exception = assertThrows(InvalidOperation.class, () -> loginServiceImpl.login(loginDto));
		assertEquals("Customer already Logged In", exception.getMessage());

	}

}
