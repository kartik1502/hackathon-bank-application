package org.training.bank.application.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.service.CustomerService;

@ExtendWith(SpringExtension.class)
class LoginControllerTest {

	@InjectMocks
	LoginController loginController;

	@Mock
	CustomerService customerService;

	@Test
	void testEnrollStudent_WithValidData_ShouldReturnCreatedStatus() {

		LoginDto loginDto = new LoginDto("C001", "Divya@514");
		Mockito.when(customerService.login(any(LoginDto.class))).thenReturn(new ResponseDto("200", "Login successful"));

		ResponseEntity<ResponseDto> response = loginController.customerLogin(loginDto);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}

}
