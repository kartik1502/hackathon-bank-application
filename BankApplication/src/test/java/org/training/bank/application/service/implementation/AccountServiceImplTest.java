package org.training.bank.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.AccountDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.AccountType;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
class AccountServiceImplTest {

	@InjectMocks
	private AccountServiceImpl accountServiceImpl;

	@Mock
	private AccountRepository accountRepository;

	@Mock
	private CustomerRepository customerRepository;

	@Test
	void UserNotFound() {

		String customerId = "C001";
		
		when(customerRepository.findById(customerId)).thenReturn(java.util.Optional.empty());
		ResourceNotFound exception = assertThrows(ResourceNotFound.class,
				() -> accountServiceImpl.getAccount(customerId));
		assertEquals("Customer Not Found", exception.getMessage());
	}

	@Test
	void login_NegativeTest_AccessDenied() {
		Customer customer = new Customer();
		String customerId = "C001";
		
		when(customerRepository.findById(customerId)).thenReturn(Optional.of(customer));

		customer.setLoginStatus(LoginStatus.LOGOUT);

		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class,
				() -> accountServiceImpl.getAccount(customerId));
		assertEquals("Login is required to get the Account Summary", exception.getMessage());

	}

	@Test
	void UserFound() {

		Customer customer = new Customer();
		customer.setCustomerId("1");
		customer.setName("John Doe");
		customer.setLoginStatus(LoginStatus.LOGIN);

		Account account1 = new Account();
		account1.setAccountNumber("123456");
		account1.setBalance(BigDecimal.valueOf(1000.0));
		account1.setAccountType(AccountType.SAVINGS);
		account1.setCustomer(customer);

		Account account2 = new Account();
		account2.setAccountNumber("789012");
		account2.setBalance(BigDecimal.valueOf(1200.0));
		account2.setAccountType(AccountType.CURRENT);
		account2.setCustomer(customer);

		when(customerRepository.findById("1")).thenReturn(java.util.Optional.of(customer));
		when(accountRepository.findAccountByCustomer(customer)).thenReturn(List.of(account1, account2));

		List<AccountDto> accounts = accountServiceImpl.getAccount("1");

		assertNotNull(accounts);
		assertEquals(2, accounts.size());

	}

}
