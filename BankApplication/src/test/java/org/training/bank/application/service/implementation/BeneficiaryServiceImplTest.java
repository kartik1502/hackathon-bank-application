package org.training.bank.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.BeneficiaryAccount;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
 class BeneficiaryServiceImplTest {
	@InjectMocks
	private BeneficiaryServiceImpl beneficiaryServiceImpl;
	@Mock
	private BeneficiaryAccountRepository beneficiaryAccountRepository;
	@Mock
	private AccountRepository accountRepository;
	@Mock
	private CustomerRepository customerRepository;

	@Test
	void testAccountNotFound() {
	
		Mockito.when(accountRepository.findById("2")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, () -> beneficiaryServiceImpl.viewBeneficiary("2"));
	}
	
	@Test
	void testCustomerNotFound() 
	{
	 Account account=Account.builder().customer(Customer.builder().customerId("1").build()).build();
	 Mockito.when(accountRepository.findById("1")).thenReturn(Optional.of(account));
	 Mockito.when(customerRepository.findById("1")).thenReturn(Optional.empty());
	
	 assertThrows(ResourceNotFound.class, () -> beneficiaryServiceImpl.viewBeneficiary("1"));
	}
	
	
	@Test
	void testCustomer_LoggedOut() {
		
		Customer customer = Customer.builder().loginStatus(LoginStatus.LOGOUT).build();
		Account account=Account.builder().customer(Customer.builder().customerId("1").build()).build();
		
		Mockito.when(accountRepository.findById(Mockito.anyString())).thenReturn(Optional.of(account));
		Mockito.when(customerRepository.findById("1")).thenReturn(Optional.of(customer));
	
		assertThrows(UnAuthorizedAccess.class, () -> beneficiaryServiceImpl.viewBeneficiary("1"));

	}
	
	@Test
    void testFindByAccount() {
         
		Account account=Account.builder().customer(Customer.builder().customerId("1").build()).build();
		 List<BeneficiaryAccount> accounts = new ArrayList<>();
		 Customer customer = Customer.builder().loginStatus(LoginStatus.LOGIN).build();
		 Mockito.when(customerRepository.findById("1")).thenReturn(Optional.of(customer));
		 Mockito.when(accountRepository.findById(Mockito.anyString())).thenReturn(Optional.of(account));
		 accounts.add(new BeneficiaryAccount(2l,"4567897","GHY4566",account));
	     Mockito.when(beneficiaryAccountRepository.findByAccount(account)).thenReturn(accounts);
	     
	     List<BeneficiaryDto> results = beneficiaryServiceImpl.viewBeneficiary("4567890");
	     assertEquals(1,results.size());
		
	

}
}
