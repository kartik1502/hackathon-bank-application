package org.training.bank.application.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.AccountDto;
import org.training.bank.application.service.AccountService;

@ExtendWith(SpringExtension.class)
class AccountControllerTest {

	@InjectMocks
	AccountController accountController;

	@Mock
	AccountService accountService;

	@Test
	void testEnrollStudent_WithValidData_ShouldReturnCreatedStatus() {
		String customerId = "C001";
		List<AccountDto> accounts = Arrays.asList(
				new AccountDto("divya", "Acc001", BigDecimal.valueOf(1000.00), "SAVINGS"),
				new AccountDto("megha", "Acc002", BigDecimal.valueOf(2000.00), "SAVINGS"));

		Mockito.when(accountService.getAccount(customerId)).thenReturn(accounts);

		ResponseEntity<List<AccountDto>> response = accountController.accountSummary(customerId);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());

	}

}
